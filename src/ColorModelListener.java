interface ColorModelListener
{
    void colorChanged(double[] XYZ, ColorModelPanel origin);
}
